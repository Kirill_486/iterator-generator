export interface IBook {
    IMDB: string;
    Title: string;
    soldCopies: number;
    publishedYear: number;
}
