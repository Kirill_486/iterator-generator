export class Logger {
    static Log(message: any) {
        let preparedMessage: string;
        if (typeof message === 'object') {
            preparedMessage = JSON.stringify(message, undefined, 2);
        } else {
            preparedMessage = message;
        }
        console.log(preparedMessage);
    }
}