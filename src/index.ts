import { IBook } from "./types/domainTypes";
import { Bookshelf } from "./BookShelf/BookShelf";
import { Logger } from "./Logger/logger";
import * as assert from 'assert';
import { ISortTypes } from "./BookShelf/types";

const books: IBook[] = [
    {
        IMDB: '123',
        Title: 'A Song of Ice and Fire',
        publishedYear: 1997,
        soldCopies: 15E06
    },    
    {
        IMDB: '789',
        Title: 'Harry Potter',
        publishedYear: 2007,
        soldCopies: 500E06,
    },
    {
        IMDB: '456',
        Title: 'The Lord of the Rings',
        publishedYear: 1968,
        soldCopies: 150E06,
    },
];

const bookShelf = new Bookshelf(books);
for (let book of bookShelf) {
    Logger.Log(book); 
}

bookShelf.setSortType(ISortTypes.BY_YEAR);
for (let book of bookShelf) {
    Logger.Log(book); 
}

bookShelf.setSortType(ISortTypes.BY_ECONOMICAL_EFFECT);
for (let book of bookShelf) {
    Logger.Log(book); 
}