export enum ISortTypes {
    BY_TITLE,
    BY_YEAR,
    BY_ECONOMICAL_EFFECT,
}
