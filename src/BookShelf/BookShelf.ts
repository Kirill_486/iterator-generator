import { IBook } from "../types/domainTypes";
import { ISortTypes } from "./types";
import { calculateEconomicEffect } from "./behavior/economicEffectHelper";
import { sortByTitle, sortByYear, sortByEconomicEffect } from "./behavior/sortMethods";

export class Bookshelf {
    private innerBookshelf: IBook[];
    private orderedBooks: IBook[];
    private sortType: ISortTypes = ISortTypes.BY_TITLE;

    constructor(books: IBook[]) {
        this.innerBookshelf = books;
        this.reorderInnerCollection();
    }

    *[Symbol.iterator]() {
        for (let book of this.orderedBooks) {
            yield book;
        }
    }

    setSortType(type: ISortTypes) {
        this.sortType = type;
        this.reorderInnerCollection();
    }

    private reorderInnerCollection() {
        switch (this.sortType) {
            case ISortTypes.BY_TITLE: {
                this.orderedBooks = this.innerBookshelf.sort(sortByTitle);
                break;
            }
            case ISortTypes.BY_YEAR: {
                this.orderedBooks = this.innerBookshelf.sort(sortByYear);
                break;
            }
            case ISortTypes.BY_ECONOMICAL_EFFECT: {
                // becouse we want the biggest sales BOOM at the top
                this.orderedBooks = this.innerBookshelf.sort(sortByEconomicEffect).reverse();
                break;
            }
            default: {
                throw new Error('Not implemented');
            }
        }
    }
}
