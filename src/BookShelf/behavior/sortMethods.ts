import { IBook } from "../../types/domainTypes";
import { calculateEconomicEffect } from "./economicEffectHelper";

export const sortByTitle =
(
    a: IBook, b: IBook
) => {
    if (a.Title > b.Title) {
        return 1;
    } else if (a.Title < b.Title) {
        return -1;
    } else {
        return 0;
    }
};

export const sortByYear = (a: IBook, b: IBook) => {
    if (a.publishedYear> b.publishedYear) {
        return 1;
    } else if (a.publishedYear < b.publishedYear) {
        return -1;
    } else {
        return 0;
    }
};

export const sortByEconomicEffect = (a: IBook, b: IBook) => {
    const aEffect = calculateEconomicEffect(a);
    const bEffect = calculateEconomicEffect(b);
    if (aEffect > bEffect) {
        return 1;
    } else if (aEffect < bEffect) {
        return -1;
    } else {
        return 0;
    }
}
