import { IBook } from "../../types/domainTypes";
import { Logger } from "../../Logger/logger";
import { copieAYear } from "./alias";

export const calculateEconomicEffect =
(
    book: IBook,
): copieAYear => {
    const dateNow =  new Date();
    const yearNow = dateNow.getFullYear();

    const yearsReleased = yearNow - book.publishedYear;
    const economicEffect = book.soldCopies / yearsReleased;

    Logger.Log(`${economicEffect} -- ${book.Title}`);

    return economicEffect;
}